import { defineConfig } from 'astro/config';

// https://astro.build/config
export default defineConfig({
  site: 'https://aaylward.gitlab.io',
  base: '/shaky-solstice',
  outDir: 'public',
  publicDir: 'static',
});
